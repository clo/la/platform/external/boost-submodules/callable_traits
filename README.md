<!--
Copyright Barrett Adair 2016
Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
-->

# CallableTraits <a target="_blank" href="https://travis-ci.org/badair/callable_traits">![Travis status][badge.Travis]</a> <a target="_blank" href="https://ci.appveyor.com/project/badair/callable-traits">![Appveyor status][badge.Appveyor]</a> <a target="_blank" href="https://wandbox.org/permlink/bN1iEyrnEBPKFJaf">![Try it online][badge.wandbox]</a> <a target="_blank" href="https://gitter.im/badair/callable_traits">![Gitter Chat][badge.Gitter]</a>

Current Version: 2.0.1

CallableTraits is a C++11 header-only library for the inspection, synthesis, and decomposition of callable types.

[Full documentation available here.](http://badair.github.io/callable_traits/index.html)

On April 17, 2017, CallableTraits was [conditionally accepted into the Boost C++ Libraries](https://lists.boost.org/Archives/boost/2017/04/234513.php). You can read more about the formal review process [here](http://www.boost.org/community/reviews.html).

[Licensed under the Boost Software License, v. 1.0](LICENSE.md)

<!-- Links -->
[badge.Appveyor]: https://ci.appveyor.com/api/projects/status/uf0l91v7l4wc4kw6/branch/master?svg=true
[badge.Gitter]: https://img.shields.io/badge/gitter-join%20chat-blue.svg
[badge.Travis]: https://travis-ci.org/badair/callable_traits.svg?branch=master
[badge.Wandbox]: https://img.shields.io/badge/try%20it-online-blue.svg

